import dotenv from 'dotenv';
import express from 'express';
import routes from './routes';

dotenv.config({
  path: process.env.NODE_ENV === 'test' ? '.env.test' : '.env',
});

class App {
  public express: express.Application;

  constructor() {
    this.express = express();

    this.midlewares();
    this.routes();
  }

  midlewares() {
    this.express.use(express.json());
  }

  routes() {
    this.express.use(routes);
  }
}

export default new App().express;
