/* eslint-disable global-require */
import Sequelize from 'sequelize';

// import databaseConfig from '../config/database';

// require('../config/database');

class Database {
  public connection: Sequelize.Sequelize;

  constructor() {
    this.connection = new Sequelize.Sequelize(require('../config/database'));
    this.init();
  }

  init(): void {
    this.connection = new Sequelize.Sequelize(require('../config/database'));
  }
}

const database: Database = new Database();

export default database;
