import express from 'express';

import authMidleware from './app/midlewares/auth';
import UsersController from './app/controllers/UsersController';
import SessionsController from './app/controllers/SessionsController';
import MoviesController from './app/controllers/MoviesController';
import RatesController from './app/controllers/RatesController';

const routes = express.Router();

const usersController = new UsersController();
const sessionController = new SessionsController();
const moviesController = new MoviesController();
const ratesController = new RatesController();

routes.post('/users', usersController.create);
routes.post('/session', sessionController.create);

routes.use(authMidleware);

routes.put('/users', usersController.update);
routes.delete('/users', usersController.delete);
routes.get('/users', usersController.index);

routes.get('/movies', moviesController.index);

routes.post('/rates', ratesController.create);
routes.get('/rates', ratesController.index);

export default routes;
