import { Request, Response, NextFunction } from 'express';
import { verify } from 'jsonwebtoken';

import authConfig from '../../config/auth';

interface TokenPayload {
  iat: number;
  exp: number;
  id: string;
}

export default function ensureAuthenticated(
  request: Request,
  response: Response,
  next: NextFunction,
): any {
  const authHeader = request.headers.authorization;

  if (!authHeader) {
    return response.status(401).json({ err: 'JWT token is missing' });
  }

  const [, token] = authHeader.split(' ');

  try {
    const decoded = verify(token, authConfig.secret as string);

    const { id } = decoded as TokenPayload;
    request.user = {
      id,
    };

    return next();
  } catch {
    return response.status(401).json({ err: 'Invalid JWT token' });
  }
}
