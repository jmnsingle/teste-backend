import { Request, Response } from 'express';
import * as Yup from 'yup';

import Rate from '../models/Rate';

export default class RatesController {
  async create(request: Request, response: Response) {
    const { title, rate } = request.body;

    const schema = Yup.object().shape({
      title: Yup.string().required(),
      rate: Yup.number().required(),
    });

    if (!(await schema.isValid(request.body))) {
      return response.status(400).json({ error: 'Validation fails' });
    }

    if (Number(rate) < 0 || Number(rate) > 4) {
      return response.status(400).json({ err: 'Ivalid rate.' });
    }

    const rateData = await Rate.create({
      user_id: request.user.id,
      title,
      rate,
    });

    return response.json(rateData);
  }

  async index(request: Request, response: Response) {
    const users = await Rate.findAll({
      where: {
        user_id: request.user.id,
      },
    });

    return response.status(200).json(users);
  }
}
