import { Request, Response } from 'express';
import axios from 'axios';

export default class MovieController {
  // eslint-disable-next-line consistent-return
  async index(request: Request, response: Response) {
    const { filters } = request.query;

    if (!String(filters).trim()) {
      return response
        .status(400)
        .json({ err: 'At least one filter is required.' });
    }

    axios
      .get('https://imdb8.p.rapidapi.com/title/find', {
        params: {
          q: `${filters}`,
        },
        headers: {
          'x-rapidapi-host': 'imdb8.p.rapidapi.com',
          'x-rapidapi-key':
            '4d2da73138msh3de1956b3142a28p1a8995jsn41d267888c83',
          useQueryString: true,
        },
      })
      .then((res: any) => {
        return response.status(200).json(res.data);
      })
      .catch(err => {
        return response.status(400).json({ err });
      });
  }
}
