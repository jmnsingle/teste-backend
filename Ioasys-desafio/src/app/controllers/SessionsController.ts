import { Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import * as Yup from 'yup';
import bcrypt from 'bcryptjs';

import User from '../models/User';
import authConfig from '../../config/auth';

export default class SessionsController {
  async create(request: Request, response: Response) {
    const schema = Yup.object().shape({
      email: Yup.string().email().required(),
      password: Yup.string().required(),
    });

    if (!(await schema.isValid(request.body))) {
      return response.status(400).json({ erro: 'Validation fails' });
    }

    const { email, password } = request.body;

    const userExist = await User.findOne({
      where: { email: request.body.email },
    });

    if (!userExist) {
      return response.status(401).json({ error: 'User not found' });
    }

    const { id, name, passwordHash } = userExist;

    if (!(await bcrypt.compare(password, passwordHash))) {
      return response.status(401).json({ error: 'Password does not match' });
    }

    return response.status(200).json({
      user: {
        id,
        name,
        email,
      },
      token: jwt.sign({ id }, authConfig.secret as string, {
        expiresIn: authConfig.expiresIn,
      }),
    });
  }
}
