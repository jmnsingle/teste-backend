import { Request, Response } from 'express';
import { isUuid } from 'uuidv4';
import * as Yup from 'yup';

import User from '../models/User';

export default class ClassesController {
  async create(request: Request, response: Response) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
      email: Yup.string().email().required(),
      password: Yup.string().required().min(6),
    });

    if (!(await schema.isValid(request.body))) {
      return response.status(400).json({ error: 'Validation fails' });
    }

    const userExist = await User.findOne({
      where: { email: request.body.email },
    });

    if (userExist) {
      return response.status(400).json({ error: 'User already exists' });
    }

    const { id, name, email } = await User.create(request.body);

    return response.json({
      id,
      name,
      email,
    });
  }

  async update(request: Request, response: Response) {
    const schema = Yup.object().shape({
      name: Yup.string(),
      email: Yup.string().email(),
      oldPassword: Yup.string().min(6),
      password: Yup.string()
        .min(6)
        .when('oldPassword', (oldPassword: string, field: any) =>
          oldPassword ? field.required() : field,
        ),
      confirmPassword: Yup.string().when(
        'password',
        (password: string, field: any) =>
          password ? field.required().oneOf([Yup.ref('password')]) : field,
      ),
    });

    if (!(await schema.isValid(request.body))) {
      return response.status(400).json({ error: 'Validation fails' });
    }

    const { email, oldPassword } = request.body;

    const user = await User.findByPk(request.user.id);

    if (!user?.admin) {
      return response
        .status(400)
        .json({ error: 'You does not have permition for this.' });
    }

    if (email !== user?.email) {
      const userExist = await User.findOne({
        where: { email },
      });

      if (userExist) {
        return response.status(400).json({ error: 'User already exists' });
      }
    }

    if (oldPassword && !(await user?.checkPassword(oldPassword))) {
      return response.status(401).json({ error: 'Password not match' });
    }

    const { id, name } = await user.update(request.body);

    return response.json({ id, name, email });
  }

  async delete(request: Request, response: Response) {
    const { id } = request.query;

    if (!isUuid(id as string)) {
      return response.status(400).json({ error: 'Invalid ID.' });
    }

    const userAdmin = await User.findByPk(request.user.id);

    if (!userAdmin?.admin) {
      return response
        .status(400)
        .json({ error: 'You does not have permition for this.' });
    }

    const userExist = await User.findOne({
      where: { id },
    });

    if (!userExist) {
      return response.status(400).json({ error: 'User not found.' });
    }

    await User.destroy({ where: { id } });

    return response.send();
  }

  async index(request: Request, response: Response) {
    const userAdmin = await User.findByPk(request.user.id);

    if (!userAdmin?.admin) {
      return response
        .status(400)
        .json({ error: 'You does not have permition for this.' });
    }

    const users = await User.findAll();

    return response.status(200).json(users);
  }
}
