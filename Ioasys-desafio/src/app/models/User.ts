import Sequelize, { Model } from 'sequelize';
import bcrypt from 'bcryptjs';
import { uuid } from 'uuidv4';

import database from '../../database';

class User extends Model {
  public id!: string;

  public name!: string;

  public admin!: boolean;

  public email!: string;

  public password!: string;

  public passwordHash!: string;

  public readonly createdAt!: Date;

  public readonly updatedAt!: Date;

  public async checkPassword(password: string): Promise<boolean> {
    return bcrypt.compare(password, this.passwordHash);
  }
}

User.init(
  {
    name: Sequelize.STRING,
    admin: Sequelize.BOOLEAN,
    email: Sequelize.STRING,
    password: Sequelize.VIRTUAL,
    passwordHash: Sequelize.STRING,
  },
  {
    sequelize: database.connection,
    // freezeTableName: true,
  },
);

User.addHook(
  'beforeSave',
  async (user: User): Promise<void> => {
    if (user.password) {
      user.passwordHash = await bcrypt.hash(user.password, 8);
      user.id = uuid();
    }
  },
);

export default User;
