import { uuid } from 'uuidv4';
import Sequelize, { Model } from 'sequelize';

import database from '../../database';

class Rate extends Model {
  public id!: string;

  public user_id!: string;

  public title!: string;

  public rate!: number;

  public readonly createdAt!: Date;

  public readonly updatedAt!: Date;
}

Rate.init(
  {
    user_id: Sequelize.STRING,
    title: Sequelize.STRING,
    rate: Sequelize.INTEGER,
  },
  {
    sequelize: database.connection,
  },
);

// Rate.associate = function (models) {
//   Rate.hasMany(models.users, {
//     foreignKey: 'user_id',
//     as: 'users_detail ',
//   });
// };

Rate.addHook(
  'beforeSave',
  async (rate: Rate): Promise<void> => {
    rate.id = uuid();
  },
);

export default Rate;
