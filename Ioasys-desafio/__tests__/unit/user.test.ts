import bcrypt from 'bcryptjs';

import User from '../../src/app/models/User';
import factory from '../utils/factories';

interface UserProps {
  name?: string;
  email?: string;
  password?: string;
  passwordHash?: string;
  checkPassword?: (password: string) => boolean;
}

describe('User', () => {
  beforeEach(async () => {
    await User.destroy({
      truncate: true,
      force: true,
    });
  });

  it('Should be able to ecrypt user password', async () => {
    const user: UserProps = await factory.create('User', {
      password: '123456',
    });

    const compareHash = await bcrypt.compare(
      '123456',
      user.passwordHash as string,
    );

    expect(compareHash).toBe(true);
  });
});
