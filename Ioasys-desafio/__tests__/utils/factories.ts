import factory from 'factory-girl';
import faker from 'faker';

import User from '../../src/app/models/User';
import Rate from '../../src/app/models/Rate';

factory.define('User', User, {
  name: faker.name.findName() as string,
  email: faker.internet.email() as string,
  password: faker.internet.password() as string,
});

factory.define('Rate', Rate, {
  Title: faker.name.findName() as string,
  rate: faker.random.number({
    min: 0,
    max: 4,
  }) as number,
});

export default factory;
