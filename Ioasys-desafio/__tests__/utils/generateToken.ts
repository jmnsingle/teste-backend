import jwt from 'jsonwebtoken';

import authConfig from '../../src/config/auth';

export default function generateToken(id: string) {
  return jwt.sign({ id }, authConfig.secret as string, {
    expiresIn: authConfig.expiresIn,
  });
}
