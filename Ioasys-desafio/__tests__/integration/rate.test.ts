import request from 'supertest';

import User from '../../src/app/models/User';
import app from '../../src/app';
import generateToken from '../utils/generateToken';
import factory from '../utils/factories';

interface UserProps {
  id?: string;
  name?: string;
  email?: string;
  password?: string;
}

interface RateProps {
  title?: string;
  rate?: number;
}

describe('Rates', () => {
  beforeEach(async () => {
    await User.destroy({
      truncate: true,
      force: true,
    });
  });

  it('Should be able to get rates', async () => {
    const user: UserProps = await factory.create('User', {
      password: '123456',
    });

    const response = await request(app)
      .get('/rates')
      .set('Authorization', `Bearer ${generateToken(user.id as string)}`);

    expect(response.status).toBe(200);
  });

  it('Should be able to create rates', async () => {
    const user: UserProps = await factory.create('User', {
      password: '123456',
    });

    const response = await request(app)
      .post('/rates')
      .send({ title: 'Inception', rate: 4 })
      .set('Authorization', `Bearer ${generateToken(user.id as string)}`);

    expect(response.status).toBe(200);
  });

  it('Should NOT be able to create rates without body', async () => {
    const user: UserProps = await factory.create('User', {
      password: '123456',
    });

    const response = await request(app)
      .post('/rates')
      .set('Authorization', `Bearer ${generateToken(user.id as string)}`);

    expect(response.status).toBe(400);
  });

  it('Should NOT be able to create rates with invalid rate', async () => {
    const user: UserProps = await factory.create('User', {
      password: '123456',
    });

    const response = await request(app)
      .post('/rates')
      .send({ title: 'Inception', rate: 5 })
      .set('Authorization', `Bearer ${generateToken(user.id as string)}`);

    expect(response.status).toBe(400);
  });
});
