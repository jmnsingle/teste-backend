import request from 'supertest';

import app from '../../src/app';
import User from '../../src/app/models/User';
import factory from '../utils/factories';
import generateToken from '../utils/generateToken';

interface UserProps {
  id?: string;
  name?: string;
  email?: string;
  password?: string;
}

describe('Authentication', () => {
  beforeEach(async () => {
    await User.destroy({
      truncate: true,
      force: true,
    });
  });

  it('Should be able to authenticate user with valid credentials', async () => {
    await factory.create('User', {
      email: 'user@email.com',
      password: '123456',
    });

    const response = await request(app).post('/session').send({
      email: 'user@email.com',
      password: '123456',
    });

    expect(response.status).toBe(200);
  });

  it('Should NOT be able to authenticate user with INVALID credentials', async () => {
    const user: UserProps = await factory.create('User', {
      password: '123456',
    });

    const response = await request(app).post('/session').send({
      email: user.email,
      password: '123457',
    });

    expect(response.status).toBe(401);
  });

  it('Should return JRT token when authenticated', async () => {
    const user: UserProps = await factory.create('User', {
      password: '123456',
    });

    const response = await request(app).post('/session').send({
      email: user.email,
      password: '123456',
    });

    expect(response.body).toHaveProperty('token');
  });

  it('Should be able to access private routes when authenticated', async () => {
    const user: UserProps = await factory.create('User', {
      password: '123456',
    });

    const response = await request(app)
      .get('/rates')
      .set('Authorization', `Bearer ${generateToken(user.id as string)}`);

    expect(response.status).toBe(200);
  });

  it('Should NOT be able to access private routes when NOT authenticated', async () => {
    await factory.create('User', {
      password: '123456',
    });

    const response = await request(app).get('/rates');

    expect(response.status).toBe(401);
  });

  it('Should NOT be able to access private routes with invalid token', async () => {
    await factory.create('User', {
      password: '123456',
    });

    const response = await request(app)
      .get('/rates')
      .set('Authorization', `Bearer 123456`);

    expect(response.status).toBe(401);
  });
});
