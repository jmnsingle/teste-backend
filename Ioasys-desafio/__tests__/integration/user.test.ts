import bcrypt from 'bcryptjs';
import request from 'supertest';

import User from '../../src/app/models/User';
import app from '../../src/app';
import factory from '../utils/factories';
import generateToken from '../utils/generateToken';

interface UserProps {
  id?: string;
  admin?: boolean;
  name?: string;
  email?: string;
  password?: string;
  passwordHash?: string;
  checkPassword?: (password: string) => boolean;
}

describe('User', () => {
  beforeEach(async () => {
    await User.destroy({
      truncate: true,
      force: true,
    });
  });

  it('Should be able to create user', async () => {
    const response = await request(app).post('/users').send({
      name: 'Juliano',
      email: 'user@email.com',
      password: '123456',
      admin: true,
    });

    expect(response.status).toBe(200);
  });

  it('Should be able to update user', async () => {
    const user: UserProps = await factory.create('User', {
      name: 'juliano',
      email: 'user@email.com',
      password: '123456',
      admin: true,
    });

    const response = await request(app)
      .put('/users')
      .send({
        name: 'Juliano Nogueira',
        email: 'user@email.com',
        oldPassword: '123456',
        password: '123456789',
        confirmPassword: '123456789',
        admin: true,
      })
      .set('Authorization', `Bearer ${generateToken(user.id as string)}`);

    expect(response.status).toBe(200);
  });

  it('Should be able to delete user', async () => {
    const user: UserProps = await factory.create('User', {
      name: 'juliano',
      email: 'user@email.com',
      password: '123456',
      admin: true,
    });

    const userToBeDeleted: UserProps = await factory.create('User');

    const response = await request(app)
      .delete('/users')
      .query({ id: userToBeDeleted.id })
      .set('Authorization', `Bearer ${generateToken(user.id as string)}`);

    expect(response.status).toBe(200);
  });

  it('Should be able to get users', async () => {
    const user: UserProps = await factory.create('User', {
      name: 'juliano',
      email: 'user@email.com',
      password: '123456',
      admin: true,
    });

    const response = await request(app)
      .get('/users')
      .set('Authorization', `Bearer ${generateToken(user.id as string)}`);

    expect(response.status).toBe(200);
  });
  // it('Should be able to compare user password', async () => {
  //   const user: UserProps = await factory.create('User', {
  //     password: '123456',
  //   });

  //   const compareHash = user.checkPassword
  //     ? user.checkPassword('1' as string)
  //     : null;

  //   expect(compareHash).toBe(true);
  // });
});
