import request from 'supertest';

import User from '../../src/app/models/User';
import app from '../../src/app';
import generateToken from '../utils/generateToken';
import factory from '../utils/factories';

interface UserProps {
  id?: string;
  name?: string;
  email?: string;
  password?: string;
}

describe('Movies', () => {
  beforeEach(async () => {
    await User.destroy({
      truncate: true,
      force: true,
    });
  });

  it('Should be able to get the movies', async () => {
    const user: UserProps = await factory.create('User', {
      password: '123456',
    });

    const response = await request(app)
      .get('/movies')
      .set('Authorization', `Bearer ${generateToken(user.id as string)}`)
      .query({ filters: 'Leonardo Di Caprio' });

    expect(response.status).toBe(200);
  });

  it('Should NOT be able to get the movies with empty filters', async () => {
    const user: UserProps = await factory.create('User', {
      password: '123456',
    });

    const response = await request(app)
      .get('/movies')
      .set('Authorization', `Bearer ${generateToken(user.id as string)}`)
      .query({ filters: '' });

    expect(response.status).toBe(400);
  });
});
